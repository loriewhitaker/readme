
### Lorie Whitaker's README

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

Please feel free to contribute to this page by opening a merge request. 

### About me

My name is Lorie Whitaker and I'm a Staff UX Researcher at GitLab focusing on the Ops section.

* [My GitLab handle[(https://gitlab.com/loriewhitaker)

### My role

My role as a Staff UX Researcher encompasses:

* Providing guidance to my peers, designers, product managers, and others when it comes to UX Research methods and practices.
* Providing a higher level of confidence for design and product decisions by leveraging UX research.

### Working with me

* I thrive on context. When you bring me a problem, I most likely will ask you to tell me how we got to where we are. What have you learned already? What do you still need to find out?

* I don't believe in process for process sake. If it's not working, let's talk about it and see how we can change it.

* I believe in discourse followed by action. 

* I'm always available for a chat! How to reach me:

  * GitLab - @ me directly in issues you need me to weigh in on. I'll do the same for you
  * Slack - mention me or DM me if you need me. I've always got Slack open during working hours and on my personal cell.

  
### Outside of work

* I live in the state of Texas in the US (Central time). Before moving to the Lone Star state, I lived in Seattle. Before that, I was born and raised in Louisiana. 
* I love to knit, play with my cat Fergus, read books most people would find boring (non-fiction about the 1918 flu pandemic, the impact of cod fishing, history of computing, etc.), bake.

* My favorite country is the UK and if I could, I'd totally live there. My dream is to have a cottage by the sea!
* I also love to garden with native and adaptive plants. I recently redesigned my front yard using my own design I created from a landscaping class I took. I'm looking forward to doing the same to my backyard!
 
